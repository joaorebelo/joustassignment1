//
//  Enemy.swift
//  5244joustassignment2
//
//  Created by Joao Rebelo on 2019-06-20.
//  Copyright © 2019 Joao Rebelo. All rights reserved.
//

import Foundation
import SpriteKit

class Enemy:SKSpriteNode{
    private var direction : String!
    private var floor : Int!
    
    
    var Direction : String!{
        get{return self.direction}
        set{self.direction = newValue}
    }
    
    var Floor : Int!{
        get{return self.floor}
        set{self.floor = newValue}
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        //self.direction = "left"
        let d = Int.random(in: 1...2)
        if (d==1) {
            self.direction = "left"
        }else{
            self.direction = "right"
        }
        
        self.floor = 0
    }
    
    func getWalkingAnimation() -> SKAction {
        var dinoTextures:[SKTexture] = []
        for i in 1...3 {
            let fileName = "slug_\(i)"
            dinoTextures.append(SKTexture(imageNamed: fileName))
            
        }
        
        // 2. Tell Spritekit to use that array to create your animation
        let walkingAnimation = SKAction.animate(
            with: dinoTextures,
            timePerFrame: 0.1)
        
        return walkingAnimation
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
