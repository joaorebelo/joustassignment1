//
//  GameScene.swift
//  5244joustassignment2
//
//  Created by Joao Rebelo on 2019-06-20.
//  Copyright © 2019 Joao Rebelo. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var background = SKSpriteNode(imageNamed: "joust_bg")
    var screenWidth = 0
    var screenHeight = 0
    var player:Player?
    var enemies:[Enemy] = []
    var floors:[CGFloat] = [225.0, 468.0, 712.0, 948.0]
    var initialMousePosition:CGPoint?
    let swipeRight = UISwipeGestureRecognizer()
    let swipeLeft = UISwipeGestureRecognizer()
    let swipeUP = UISwipeGestureRecognizer()
    let swipeDown = UISwipeGestureRecognizer()
    
    var livesLabel:SKLabelNode!
    var scoreLabel:SKLabelNode!
    
    private static var killEnemy: AVAudioPlayer?
    var moveFloor: AVAudioPlayer?
    
    override func didMove(to view: SKView) {
        
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.name = "wall"
        self.physicsBody?.categoryBitMask = 2
        
        //self.anchorPoint = CGPoint(x: 0, y: 0)
        
        // MARK: setup contact delegate
        self.physicsWorld.contactDelegate = self
        
        
        //background
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        self.player = Player(imageNamed:"penguin_walk01")
        //print("width: \(self.frame.width)")
        //print("height: \(self.frame.height)")
        
        self.player!.position.x = self.size.width/2;
        self.player!.position.y = 225.0;
        
        self.player!.zPosition = 99;
        
        self.player!.physicsBody = SKPhysicsBody(rectangleOf:(self.player?.size)!)
        self.player!.physicsBody!.affectedByGravity = true
        self.player!.physicsBody!.isDynamic = true
        self.player!.physicsBody!.allowsRotation = false
        self.player?.physicsBody?.categoryBitMask = 4
        self.player?.physicsBody?.collisionBitMask = 8
        addChild(player!)
        
        
        // MARK: Get the labels livesLabel from SKS file
        self.livesLabel = self.childNode(withName: "livesLeftLbl") as! SKLabelNode
        self.livesLabel.text = "Lives: \(self.player!.LivesLeft!)"
        self.livesLabel.fontName = "Avenir-Bold"
        
        //MARK: get label score from sks file
        self.scoreLabel = self.childNode(withName: "scoreCountLbl") as! SKLabelNode
        self.scoreLabel.text = "\(self.player!.Score!)"
        self.scoreLabel.fontName = "Avenir-Bold"
        
        var countFloors = 0
        //fill enemies array
        //for i in 1...8 {
        for i in 1...8 {
            let enemy:Enemy = Enemy(imageNamed:"slug_1")
            enemy.name = "enemy"
            //print("width: \(self.frame.width)")
            //print("height: \(self.frame.height)")
            if(i == 3 || i == 5 || i == 7){
                countFloors+=1;
            }
            
            if(i == 0){
                enemy.position.x = CGFloat(10); //self.size.width/2;
                
            }else if(i == 1){
                enemy.position.x = CGFloat(self.size.width-enemy.size.width); //self.size.width/2;
            }else{
                let enemyXSpawn = Int.random(in: 10...(Int)(self.size.width-enemy.size.width))
                enemy.position.x = CGFloat(enemyXSpawn); //self.size.width/2;
            }
            
            enemy.position.y = self.floors[countFloors];
            enemy.size = self.player!.size
            
            enemy.zPosition = 99;
            
            enemy.physicsBody = SKPhysicsBody(rectangleOf:(enemy.size))
            enemy.physicsBody!.affectedByGravity = true
            enemy.physicsBody!.isDynamic = true
            enemy.physicsBody!.allowsRotation = false
            
            
            // set bitmasks for enemy
            enemy.physicsBody?.categoryBitMask = 1
            enemy.physicsBody?.contactTestBitMask = 3
            enemy.physicsBody?.collisionBitMask = 10
            
            addChild(enemy)
            
            self.enemies.append(enemy)
            
            // MARK: Initial create of enemy animation
            let walking:SKAction = enemy.getWalkingAnimation()
            enemy.run(SKAction.repeatForever(walking))
        }
        
        //swipe
        swipeRight.addTarget(self, action: #selector(GameScene.swipeRightFunc))
        swipeRight.direction = .right
        self.view?.addGestureRecognizer(swipeRight)
        
        swipeLeft.addTarget(self, action: #selector(GameScene.swipeLeftFunc))
        swipeLeft.direction = .left
        self.view?.addGestureRecognizer(swipeLeft)
        
        swipeUP.addTarget(self, action: #selector(GameScene.swipeUpFunc))
        swipeUP.direction = .up
        self.view?.addGestureRecognizer(swipeUP)
        
        swipeDown.addTarget(self, action: #selector(GameScene.swipeDownFunc))
        swipeDown.direction = .down
        self.view?.addGestureRecognizer(swipeDown)
    }
    
    func spawnEnemy() {
        let enemy:Enemy = Enemy(imageNamed:"slug_1")
        enemy.name = "enemy"
        //print("width: \(self.frame.width)")
        //print("height: \(self.frame.height)")
        
        let enemyXSpawn = Int.random(in: 10...(Int)(self.size.width-enemy.size.width))
        enemy.position.x = CGFloat(enemyXSpawn); //self.size.width/2;
        enemy.position.y = self.floors[Int.random(in: 0...3)];
        enemy.size = self.player!.size
        
        enemy.zPosition = 99;
        
        enemy.physicsBody = SKPhysicsBody(rectangleOf:(enemy.size))
        enemy.physicsBody!.affectedByGravity = true
        enemy.physicsBody!.isDynamic = true
        enemy.physicsBody!.allowsRotation = false
        
        
        // set bitmasks for enemy
        enemy.physicsBody?.categoryBitMask = 1
        enemy.physicsBody?.contactTestBitMask = 3
        enemy.physicsBody?.collisionBitMask = 10
        
        addChild(enemy)
        
        self.enemies.append(enemy)
        
        // MARK: Initial create of enemy animation
        let walking:SKAction = enemy.getWalkingAnimation()
        enemy.run(SKAction.repeatForever(walking))
    }
    
    // MARK: Detect when 2 things touch each other
    func didBegin(_ contact: SKPhysicsContact) {
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        print("nodeA: \(nodeA?.name)")
        print("nodeB: \(nodeB?.name)")
        
        if (nodeA?.name == "enemy" && nodeB?.name == "wall") {
            let enemy = nodeA as! Enemy
            if (enemy.Direction == "left") {
                // change L -> R
                enemy.Direction = "right"
            }
            else {
                // change R -> L
                enemy.Direction = "left"
            }
            
        }
        
        
        if (nodeA?.name == "wall" && nodeB?.name == "enemy") {
            let enemy = nodeB as! Enemy
            if (enemy.Direction == "left") {
                // change L -> R
                enemy.Direction = "right"
            }
            else {
                // change R -> L
                enemy.Direction = "left"
            }
            
        }
        
        
        if (nodeA?.name == "enemy" && nodeB?.name == "enemy") {
            print("enemy touched another enemy")
            let enemy1 = nodeA as! Enemy
            let enemy2 = nodeB as! Enemy


            // if both are facing same direction
            if (enemy1.Direction == enemy2.Direction) {
                /*print("+++++++ FACING SAME DIRECTION: \(enemy1.Direction)")
                
                if(enemy1.Direction == "left"){
                    print("+++++++ FACING LEFT+++++++++++=")
                    if (enemy1.position.x > enemy2.position.x) {
                        enemy1.Direction = "right"
                        
                    }else{
                        enemy2.Direction = "right"
                    }
                }*/
            }
            else if (enemy1.Direction != enemy2.Direction) {
                print("@@@@@@@  DIFFERENT DIRECTION!")
                if (enemy1.Direction == "left" ) {
                    enemy1.Direction = "right"
                }
                else if (enemy1.Direction == "right") {
                    enemy1.Direction = "left"
                }
                if (enemy2.Direction == "left") {
                    enemy2.Direction = "right"
                }
                else if (enemy2.Direction == "right") {
                    enemy2.Direction = "left"
                }
            }

            // if they afacing different dicrion



        }
        print("---------")
    }
    
    @objc func swipeRightFunc() {
        self.player!.Direction = "right"
        let walking:SKAction = player!.getWalkingAnimation()
        self.player!.run(SKAction.repeatForever(walking))
    }
    
    @objc func swipeLeftFunc() {
        self.player!.Direction = "left"
        let walking:SKAction = player!.getWalkingAnimation()
        self.player!.run(SKAction.repeatForever(walking))
    }
    
    @objc func swipeUpFunc() {
        playMoveFloor()
        let teleport:SKAction = player!.getTeleportAnimation()
        
        if (self.player!.Floor < 3) {
            self.player!.run(SKAction.repeat(teleport, count: 3))
            self.player!.Floor = self.player!.Floor + 1
            let m1 = SKAction.moveTo(y: floors[self.player!.Floor], duration: 1)
            
            self.player!.run(m1)
        }else{
            self.player!.Floor = 0
            self.player!.position.y = self.floors[self.player!.Floor]
        }
    }
    
    @objc func swipeDownFunc() {
        playMoveFloor()
        
        let teleport:SKAction = player!.getTeleportAnimation()
        if (self.player!.Floor > 0 ) {
            self.player!.run(SKAction.repeat(teleport, count: 3))
            self.player!.Floor = self.player!.Floor - 1
            let m1 = SKAction.moveTo(y: floors[self.player!.Floor], duration: 1)
            
            self.player!.run(m1)
        }else{
            self.player!.Floor = 3
            self.player!.position.y = self.floors[self.player!.Floor]
        }
    }
    
    func playMoveFloor(){
        run(SKAction.sequence([SKAction.playSoundFileNamed("movefloor.wav", waitForCompletion: false),SKAction.wait(forDuration: 5.0), SKAction.removeFromParent()]))

        //playSoundEffect(named: "movefloor")
        /*
        if #available(iOS 9, *) {
            let pling = SKAudioNode(fileNamed: "movefloor.wav")
            // this is important (or else the scene starts to play the sound in
            // an infinite loop right after adding the node to the scene).
            pling.autoplayLooped = false
            self.addChild(pling)
            self.run(SKAction.sequence([
                SKAction.wait(forDuration: 0.5),
                SKAction.run {
                    // this will start playing the pling once.
                    pling.run(SKAction.play())
                }
                ]))
        }
        else {
            // do it the old way
        }*/
        
        /*
        let file = SKAudioNode(fileNamed: "movefloor.wav")
        
        addChild(file)*/
        /*
        do {
            if let fileURL = Bundle.main.url(forResource: "movefloor", withExtension: "wav") {
                moveFloor = try AVAudioPlayer(contentsOf: fileURL)
            } else {
                print("No file with specified name exists")
            }
        } catch let error {
            print("Can't play the audio file failed with an error \(error.localizedDescription)")
        }
        
        
        
        moveFloor?.play()*/
    }
    
    func playKillEnemyFloor(){
        run(SKAction.sequence([SKAction.playSoundFileNamed("killenemy.wav", waitForCompletion: false),SKAction.wait(forDuration: 5.0), SKAction.removeFromParent()]))
    }
    
    func playPickEgg(){
        run(SKAction.sequence([SKAction.playSoundFileNamed("pickegg.wav", waitForCompletion: false),SKAction.wait(forDuration: 5.0), SKAction.removeFromParent()]))
    }
    
    func playWin(){
        run(SKAction.sequence([SKAction.playSoundFileNamed("win.wav", waitForCompletion: false),SKAction.wait(forDuration: 5.0), SKAction.removeFromParent()]))
    }
    
    func playLoose(){
        run(SKAction.sequence([SKAction.playSoundFileNamed("loose.wav", waitForCompletion: false),SKAction.wait(forDuration: 5.0), SKAction.removeFromParent()]))
    }
    
    /*
    open func playSoundEffect(named fileName: String) {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "wav") {
            soundPlayer = try? AVAudioPlayer(contentsOf: url)
            soundPlayer.stop()
            soundPlayer.numberOfLoops = 1
            soundPlayer.prepareToPlay()
            soundPlayer.play()
        }
    }
    */
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let mousePosition = touches.first?.location(in: self) else {
            return
        }//print(mousePosition)
        initialMousePosition = mousePosition
        
        //self.player!.run(SKAction.playSoundFileNamed("movefloor.wav",waitForCompletion:false));
        
        /*let CENTER = self.size.width / 2
        if (mousePosition.x < CENTER) {
            self.player!.Direction = "left"
            let walking:SKAction = player!.getWalkingAnimation()
            self.player!.run(SKAction.repeatForever(walking))
        }
        else {
            self.player!.Direction = "right"
            let walking:SKAction = player!.getWalkingAnimation()
            self.player!.run(SKAction.repeatForever(walking))
        }
        */
        //print("click")
    }
    
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        //print("moved")
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //print("ended")

        let locationTouched = touches.first
        
        if (locationTouched == nil) {
            // This is error handling
            // Sometimes the mouse detection doesn't work properly
            // and IOS can't get the position.
            return
        }
        
        let mousePosition = locationTouched!.location(in:self)

        // calculate those math variables (d, xd, yd)
        // (x2-x1)
        let a = mousePosition.x - self.initialMousePosition!.x
        // (y2-y1)
        let b = mousePosition.y - self.initialMousePosition!.y
        // d
        let d = sqrt( (a*a) + (b*b))
        //print("new position: \(self.player!.position.y)")
        if (d < 50.0) {
            let jumpAction = SKAction.applyImpulse(
                CGVector(dx:0, dy:160),
                duration: 0.2)
            
            self.player!.run(jumpAction)
        }
        
    }

    var eggs:[Egg] = []
    var eggsD:[Int : Egg] = [:]
    var enemiesD:[Int : Enemy] = [:]
    
    override func update(_ currentTime: TimeInterval) {
        
        eggsD = [:]
        for (index, e) in self.eggs.enumerated() {
            eggsD[index] = e
        }
        
        
        for (index, e) in eggsD {
            if (currentTime - e.SpawnTime > 10) {
                print("EGG IS ON SCREEN FOR 5 SECONDS")
                spawnEnemy()
                e.removeFromParent()
                eggsD.removeValue(forKey: index)
            }
        }
        eggs = []
        for (_, e) in eggsD {
            eggs.append(e)
        }
        
        
        //Start Move Player
        if(self.player?.Direction == ""){
            //nothing happens
        }else if(self.player?.Direction == "left"){
            //mirror image
            let rightHandUpAction = SKAction.scaleX(to: -1, duration: 0)
            self.player!.run(rightHandUpAction)
            //move
            player!.position.x -= 5
        }else if(self.player?.Direction == "right"){
            //mirror image
            let leftHandUpAction = SKAction.scaleX(to: 1, duration: 0)
            self.player!.run(leftHandUpAction)
            //move
            player!.position.x += 5
        }
        //End Move Player
        
        if(((self.player?.position.x)! >= (self.size.width)) && self.player!.Direction! == "right"){
            self.player!.position.x = 0 + (player!.physicsBody!.velocity.dx)
        }else if(((self.player?.position.x)! <= (0)) && self.player!.Direction! == "left"){
            player?.position.x = self.size.width - (player?.physicsBody?.velocity.dx)!
        }
        //End Player Border Verification
        
        
        //------------------------------------------------------------
        //MARK: Start Move Enemies
        if (enemies.count > 0) {
            for e in enemies {
                
                if(e.Direction == "left"){
                    e.position.x -= 5

                    let rightHandUpAction = SKAction.scaleX(to: 1, duration: 0)
                    e.run(rightHandUpAction)
                    //move
                }else if(e.Direction == "right"){
                    let leftHandUpAction = SKAction.scaleX(to: -1, duration: 0)
                    e.run(leftHandUpAction)
                    //move
                    e.position.x += 5
                }

                //Start Enemies Border Verification
                if(((e.position.x) >= (self.size.width)) && e.Direction! == "right"){
                    e.position.x = 0 + (e.physicsBody!.velocity.dx)
                }else if(((e.position.x) <= (0)) && e.Direction! == "left"){
                    e.position.x = self.size.width - (e.physicsBody?.velocity.dx)!
                }
               // End Enemies Border Verification
            }
        }
        //End Move Enemies
        
        
        //Start Colisions
        enemiesD = [:]
        for (index, enemy) in self.enemies.enumerated() {
            enemiesD[index] = enemy
        }

        for (index, enemy) in enemiesD {
            if((self.player?.intersects(enemy))!){
                if(self.player!.position.y > (enemy.position.y + (enemy.size.height/2))){
                    print("kill enemy")
                    playKillEnemyFloor()
                    enemiesD.removeValue(forKey: index)
                    enemy.removeFromParent()
                    let egg:Egg = Egg(imageNamed: "egg")
                    egg.name = "egg"
                    egg.SpawnTime = currentTime
                    egg.position.x = 500
                    egg.position.y = 1000
                    egg.size = self.player!.size
                    
                    let eggXSpawn = Int.random(in: 10...(Int)(self.size.width-egg.size.width))
                    egg.position.x = CGFloat(eggXSpawn); //self.size.width/2;
                    var foundFloor = false
                    while(!foundFloor){
                        let floorXSpawn = Int.random(in: 0...3)
                        if(floorXSpawn != self.player?.Floor){
                            egg.position.y = self.floors[floorXSpawn]+50;
                            foundFloor = true
                        }
                    }
                    
                    addChild(egg)
                    eggs.append(egg)
                    
                }else{
                    print("died by the enemy")
                    
                    if(self.player!.LivesLeft > 0){
                        self.player!.LivesLeft -= 1;
                        //self.player!.position.x = self.size.width/2
                        enemiesD.removeValue(forKey: index)
                        enemy.removeFromParent()
                        playKillEnemyFloor()
                        self.livesLabel.text = "Lives Left: \(self.player!.LivesLeft!)"
                    }else{
                        playLoose()
                        print("loose screen")
                        let scene = SKScene(fileNamed:"LooserScene")
                        if (scene == nil) {
                            print("Error loading level")
                            return
                        }
                        else {
                            scene!.scaleMode = .aspectFill
                            view?.presentScene(scene!)
                        }
                    }
                }
            }
        }
        
        enemies = []
        for (_, enemy) in enemiesD {
            enemies.append(enemy)
        }
        
        eggsD = [:]
        for (index, e) in self.eggs.enumerated() {
            eggsD[index] = e
        }
        
        for (index, e) in eggsD {
            if(self.player!.intersects(e)){
                playPickEgg()
                eggsD.removeValue(forKey: index)
                e.removeFromParent()
                //score
                self.player!.Score += 150
                self.scoreLabel.text = "\(self.player!.Score!)"
                print("***********score")
            }
        }
        
        eggs = []
        for (_, e) in eggsD {
            eggs.append(e)
        }
        
        //MARK: Win Senario
        if(self.enemies.count == 0 && self.eggs.count == 0 ){
            playWin()
            print("Win screen")
            let scene = SKScene(fileNamed:"WinnerScene")
            if (scene == nil) {
                print("Error loading level")
                return
            }
            else {
                scene!.scaleMode = .aspectFill
                view?.presentScene(scene!)
            }
        }
        //end win senario
    }
}
