//
//  Egg.swift
//  5244joustassignment2
//
//  Created by Joao Rebelo on 2019-06-20.
//  Copyright © 2019 Joao Rebelo. All rights reserved.
//
import Foundation
import SpriteKit

class Egg:SKSpriteNode{
    private var eggNode:SKSpriteNode!
    private var spawnTime:TimeInterval!
    
    var EggNode : SKSpriteNode!{
        get{return self.eggNode}
        set{self.eggNode = newValue}
    }
    var SpawnTime : TimeInterval!{
        get{return self.spawnTime}
        set{self.spawnTime = newValue}
    }
    
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        self.eggNode = SKSpriteNode(imageNamed:"egg")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
