//
//  Player.swift
//  5244joustassignment2
//
//  Created by Joao Rebelo on 2019-06-20.
//  Copyright © 2019 Joao Rebelo. All rights reserved.
//

import Foundation
import SpriteKit


//typealias priceStore = (store: Store, productPrice: Double)

class Player:SKSpriteNode{
    private var direction : String!
    private var score : Int!
    private var livesLeft : Int!
    private var floor : Int!
    
    var Direction : String!{
        get{return self.direction}
        set{self.direction = newValue}
    }
    
    var Score : Int!{
        get{return self.score}
        set{self.score = newValue}
    }
    
    var LivesLeft : Int!{
        get{return self.livesLeft}
        set{self.livesLeft = newValue}
    }
    
    var Floor : Int!{
        get{return self.floor}
        set{self.floor = newValue}
    }
    
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        
   // init(playerNode : SKSpriteNode, direction : String, score : Int, livesLeft : Int) {

        self.direction = ""
        self.score = 0
        self.livesLeft = 4
        self.floor = 0
    }
    
    func getWalkingAnimation() -> SKAction {
        var dinoTextures:[SKTexture] = []
        for i in 1...4 {
            let fileName = "penguin_walk0\(i)"
            ///print("Adding: \(fileName) to array")
            dinoTextures.append(SKTexture(imageNamed: fileName))
            
        }
        
        // 2. Tell Spritekit to use that array to create your animation
        let walkingAnimation = SKAction.animate(
            with: dinoTextures,
            timePerFrame: 0.1)
        
        return walkingAnimation
        

    }
    
    func getTeleportAnimation() -> SKAction {
        var dinoTextures:[SKTexture] = []
        for i in 1...2 {
            let fileName = "penguin_slide0\(i)"
            ///print("Adding: \(fileName) to array")
            dinoTextures.append(SKTexture(imageNamed: fileName))
            
        }
        
        // 2. Tell Spritekit to use that array to create your animation
        let teleportAnimation = SKAction.animate(
            with: dinoTextures,
            timePerFrame: 0.1)
        
        return teleportAnimation
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
