//
//  LooserScene.swift
//  5244joustassignment2
//
//  Created by MacStudent on 2019-06-21.
//  Copyright © 2019 Joao Rebelo. All rights reserved.
//

import SpriteKit
import GameplayKit

class LooserScene: SKScene{
    override func didMove(to view: SKView) {
        print("Loaded loose game")
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let scene = SKScene(fileNamed:"GameScene")
        if (scene == nil) {
            print("Error loading level")
            return
        }
        else {
            scene!.scaleMode = .aspectFill
            view?.presentScene(scene!)
        }
    }
}
